﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class frmHola : Form
    {
        public frmHola()
        {
            InitializeComponent();
        }

        private void btnHola_Click(object sender, EventArgs e)
        {
            label1.Text = "Hola " + textBox1.Text;
            panel1.BackColor = Color.Red;
            label1.ForeColor = Color.White;
        }

        private void btnAdios_Click(object sender, EventArgs e)
        {
            label1.Text = "Adios " + textBox1.Text;
            panel1.BackColor = Color.Blue;
            label1.ForeColor = Color.White;
        }
    }
}
